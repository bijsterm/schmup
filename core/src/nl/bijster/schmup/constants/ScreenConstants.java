package nl.bijster.schmup.constants;

public final class ScreenConstants {

    private ScreenConstants() {
        // prevent instantiation
    }

    public static final boolean PREFER_2ND_MONITOR = false;

    // FIXME: still needed ?
//    private static final int SCREEN_FACTOR = 3;
    public static final int SCREEN_HEIGHT = 1080;
    public static final int SCREEN_WIDTH = SCREEN_HEIGHT * 10 / 16;

    public static final int FRAMES_PER_SECOND = 60;

    // FIXME: Window size in full-screen mode
    // https://stackoverflow.com/questions/56308283/libgdx-setting-the-full-screen-resolution-during-runtime-causes-application-to
    public static final boolean FULL_SCREEN = false;

    // FIXME: Leave on true during development
    public static final boolean DECORATION = true;
}
