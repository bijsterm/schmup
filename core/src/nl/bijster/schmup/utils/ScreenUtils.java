package nl.bijster.schmup.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.GridPoint2;

public final class ScreenUtils {

    private ScreenUtils() {
        // Prevent instantiation
    }

    /**
     * clearScreenForNextFrame, sets the backGroundColor to black
     * and clears the screen for the next frame to be built
     */
    public static void clearScreenForNextFrame() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    public static Lwjgl3ApplicationConfiguration getApplicationConfiguration(final int width,
                                                                             final int height,
                                                                             final int fps,
                                                                             final boolean isFullScreen,
                                                                             final boolean isDecorated,
                                                                             final boolean secondScreenPreferred) {
        final Graphics.Monitor[] monitors = Lwjgl3ApplicationConfiguration.getMonitors();

        // Continue if there is at least 1 monitor only
        if (monitors.length == 0) {
            return null;
        }

        Graphics.Monitor currentMonitor = monitors[0];
        if (monitors.length > 1 && secondScreenPreferred) {
            currentMonitor = monitors[1];
        }
        final Graphics.DisplayMode displayMode = Lwjgl3ApplicationConfiguration.getDisplayMode(currentMonitor);

        final GridPoint2 screenCoordinates = new GridPoint2();
        screenCoordinates.x = (currentMonitor.virtualX + (displayMode.width / 2)) - (width / 2);
        screenCoordinates.y = currentMonitor.virtualY + displayMode.height / 2 - height / 2;

        final Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setWindowedMode(width, height);
        config.setWindowPosition(screenCoordinates.x, screenCoordinates.y);
        config.setForegroundFPS(fps);
        config.setDecorated(isDecorated);
        if (isFullScreen) {
            config.setFullscreenMode(displayMode);
        }

        return config;
    }

}
