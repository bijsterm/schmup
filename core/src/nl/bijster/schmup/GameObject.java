package nl.bijster.schmup;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

import java.util.List;

public abstract class GameObject extends Sprite {

    // For collision detection
    private Rectangle hitbox;
    private GameObject hasCollidedWith;
    public abstract void update();

    public abstract List<Sprite> getSpritesToBeDrawn();

}
