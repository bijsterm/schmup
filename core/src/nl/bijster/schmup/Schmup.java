package nl.bijster.schmup;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import nl.bijster.schmup.player.Player;

import java.util.List;

import static nl.bijster.schmup.utils.ScreenUtils.clearScreenForNextFrame;

public class Schmup extends ApplicationAdapter {

    private SpriteBatch batch;
    private Player player;

    @Override
    public void create() {
        batch = new SpriteBatch(); // Expensive operation, so do the only once !
        player = new Player();
    }

    @Override
    public void render() {
        clearScreenForNextFrame();
        player.update();

        final List<Sprite> playerSprites = player.getSpritesToBeDrawn();
        batch.begin();
              playerSprites.forEach(sprite -> sprite.draw(batch));
        batch.end();
    }


    @Override
    public void dispose() {
        batch.dispose();
    }


}
