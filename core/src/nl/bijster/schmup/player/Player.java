package nl.bijster.schmup.player;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import nl.bijster.schmup.GameObject;

import java.util.ArrayList;
import java.util.List;

public class Player extends GameObject {

    private final Sprite playerSprite;
    private final List<Sprite> playerSpriteList = new ArrayList<>();

    private float rotation = 0f;
    private final float rotationSpeed = 1f;

    public Player() {
        playerSprite = new Sprite(new Texture("player/Player_Ship.png"));
        playerSprite.setOriginCenter(); // Set the origin for rotation & scaling
    }

    @Override
    public void update() {
        playerSprite.setPosition(50f, 50f);

        rotation = Math.max(rotation + rotationSpeed, 360f);
        playerSprite.setRotation(rotation);
    }

    @Override
    public List<Sprite> getSpritesToBeDrawn() {
        playerSpriteList.clear();
        playerSpriteList.add(playerSprite);
        return playerSpriteList;
    }
}
