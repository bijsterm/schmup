package nl.bijster.schmup.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import nl.bijster.schmup.Schmup;
import nl.bijster.schmup.constants.ScreenConstants;
import nl.bijster.schmup.utils.ScreenUtils;

import java.util.Objects;

public final class DesktopLauncher {

    private DesktopLauncher() {
    }

    public static void main(final String[] arg) {

        final Lwjgl3ApplicationConfiguration config = ScreenUtils
                .getApplicationConfiguration(ScreenConstants.SCREEN_WIDTH,
                                              ScreenConstants.SCREEN_HEIGHT,
                                              ScreenConstants.FRAMES_PER_SECOND,
                                              ScreenConstants.FULL_SCREEN,
                                              ScreenConstants.DECORATION,
                                              ScreenConstants.PREFER_2ND_MONITOR);

        // Continue if there is at least 1 monitor only
        if (Objects.isNull(config)) {
            // Oh, oh
            System.err.println("No monitor found!");
        } else {
            new Lwjgl3Application(new Schmup(), config);
        }
    }

}
